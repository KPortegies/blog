﻿
#Blog
<h4> 30 augustus 2017 </h4>
Het eerste college van het schooljaar, over games en hoe ze in elkaar zitten. We hebben geleerd hoe de basisprincipes van games in elkaar zitten en hoe je een game aantrekkelijk maakt. Ik vond het een hele game presentatie.

<h4> 04 september 2017 </h4>
Vandaag hebben we de kick-off van de eerste Desigh Challenge gehad. We hebben geleerd wat een Design Challenge inhoud. Ook kregen we op een basaal niveau uitleg over wat een ontwerper precies doet. Daarnaast kregen we te horen wat de opdracht is en wat er van ons verwacht wordt.

Na de presentatie gingen we aan de slag met wat onderzoeken had moeten zijn. Alleen begonnen we gelijk met brainstormen en zijn we daar tot de pauze mee doorgegaan. Na de pauze bedachten we dat we moesten beginnen met een onderzoek opstellen en dat hebben we ook gedaan. 

We hebben gekozen voor de doelgroep eerstejaars studenten van de opleiding Leisure en evenementmanagement van de Willem de Kooning acedemie.

Hierbij hebben we het thema 'evenementenlocaties in Rotterdam' gekozen. Dit leek ons een thema wat onze doelgroep ook zou aanspreken. 
Ons onderzoek hebben we opgedeeld in vijf delen: Het Thema, De Doelgroep, De Opdrachtgever, Games en Gebruikersvriendelijkheid. Ieder teamlid heeft een deel van het onderzoek gekregen. Ik zelf ben begonnen met onderzoek naar Het Thema wat over de evenementlocaties te Rotterdam ging. Hiermee ben ik de rest van de dag mee bezig geweest.

<h4> 05 september 2017 </h4>
Vandaag hebben we ons eerste hoorcollege over design theorie gehad.
Het ging over de basisprincipes van design en hoe je het kan interpreteren. 

<h4> 06 september 2017 </h4>
's Ochtends zijn we met het groepje verdergegaan met het onderzoek. Tegen de middag waren de onderzoeken zo goed als klaar dus hebben we ze bij elkaar gelegd om conclusies te trekken. Om 13.00 hebben we onze eerst workshop over bloggen gehad. Ik heb hier geleerd wat HTML inhoud en waarom we HTML gebruiken. De workshop ging voornamelijk over hoe je een Workdown document kan opstellen en bewerken. Na de workshop hebben we onze onderzoeken afgerond en hebben we interviewvragen opgesteld voor de interviews die we donderdag zullen afnemen.

<h4> 07 september 2017 </h4>
Vandaag hebben we een werkcollege gehad over design theorie. Het doel van het college was om in te zien dat iedereen in het dagelijks leven al ontwerpt en al gebruik maakt van iteraties ook al is het onbewust. We moesten hiervan een voorbeeld geven en daar over presenteren. Het voorbeeld wat we gekozen hadden was een probleem wat ik had toen ik op het terras aan het werk was (bediening) en er een barbecue was georganiseerd en gasten die niet bij de barbecue hoorden in de rij gingen staan omdat ze dachten dat het publiek was. Hierbij was het onderzoek een observatie en de oplossing een bord bij de ingang met informatie

<h4> 11 september 2017 </h4>
Vandaag hebben we veel gebrainstormd en hebben we ons ontwerp verder uitgewerkt en er een paper prototype van gemaakt.
Ons definitieve ontwerp voor de 1e iteratie hebben we ontworpen voor de eerstejaars studenten van de opleiding leisure en evenementmanagement aan de Willem de Kooning academie. We hebben hierbij het thema "evenementlocaties in Rotterdam" gekozen. Dit thema leek ons een goede opstap naar de studie en helpt je Rotterdam leren kennen. Het doel van het spel is om met alle eerstejaars een feest te organiseren. De locatie is van te voren al afgehuurd via de HRO, alleen moet er nog drank, eten en versiering geregeld worden. Dit is wat de studenten kunnen winnen met het spel. Ze gaan van locatie naar locatie in een groepje om gezamenlijk vragen te beantwoorden. Om er voor te zorgen dat zit gezamenlijk gaat krijgt ieder één antwoord op zijn mobiel en een iemand de vraag. hoe beter je deze vragen beantwoord, des te meer punten je verdient. Met deze punten kan je coupons kopen die je kan inwisselen bij winkels voor drank, eten en versiering. Als je bij alle locaties bent geweest krijg je de locatie van het feest en kan je je spullen meenemen en lekker feesten.
<h4> 12 september 2017 </h4>
hoorcollege symetrisch etc.
<h4> 13 september 2017 </h4>
Vandaag hebben we ons paper prototype uitgewerkt en er een spelanalyse van gemaakt. Dit hebben we gedaan met de hulp van meerdere medestudenten. Uit het spelanalyse kwam vooral naar voren dat ons spel voor het grootste deel duidelijk was en dat de spelers het einddoel van ons spel erg leuk vonden. Het deel van de coupons was alleen niet duidelijk genoeg en bracht veel verwarring op tafel.
<h4> 18 september 2017 </h4>
Dag van de presentaties en K.Y.D. We begonnen de ochtend met presentaties over de eerste iteratie van de design challenge. Onze presentatie ging redelijk goed. De feedback hield in dat we meer onderzoek moesten doen naar het budget wat we hadden. Het interactieve deel was ook erg goed, maar de speurtocht/quiz combinatie was natuurlijk niet erg origineel. Na de presentaties begonnen we met de lunch klaarzetten. Voordat we begonnen werd ons verteld dat we onze darlings moesten killen dus de sfeer zat er lekker in. Na de lunch zijn we dus opnieuw begonnen met het project maar hebben we niet veel voor mekaar gekregen. 
<h4> 19 september 2017 </h4>
Vandaag hadden we een hoorcollege over prototyping. Het ging heel erg over hoe je je spel zo makkelijk mogelijk speelbaar kan laten maken door middel van papieren prototypes.
<h4> 20 september 2017 </h4>
Ik was vandaag ziek dus ik heb vanuit huis wat individuele opdrachten gedaan.
<h4> 25 september 2017 </h4>
Vandaag hebben we allereerst instructies gekregen over het ontwerpproces en hoe we deze visueel moeten maken. Hiervoor kregen we een "ontwerpproceskaart." Deze hebben we na/tijdens de pauze met het hele team gemaakt. Voor de pauze ben ik begonnen om een takenlijst en een planning in elkaar te zetten. Na de pauze zijn we begonnen met brainstormen voor de 2e iteratie. We hebben hiervoor sticky notes gebruikt. Eerst hebben we allemaal ideeën opgeschreven en deze hebben we vervolgens in drie categorieën geplaatst; leuk, niet leuk en neutraal. We hebben hieruit een paar beginnetjes voor concepten kunnen halen waar we ons volgende keer in kunnen verdiepen.
<h4> 27 september 2017 </h4>
We begonnen vandaag met studiecoaching. We kregen opdrachten om in een kring te gaan staan en dan van groot naar klein/oud naar jong/ alfabetische volgorde naam etc. Vervolgens kregen we een formuliertje waarmee we feedback op onze teamgenoten moesten geven. Vervolgens zijn we verder aan de design challenge gegaan. We hebben de drie relevant spellen geanalyseerd. We hebben gekozen voor memory, kwartet en clash of clans.

<h4> 2 oktober 2017 </h4>
We hebben vandaag ons concept uitgewerkt en daarbij feedback gevraagd. De feedback was positief, maar er kon nog war meer interactie tussen de groepjes bij.

<h4> 4 oktober 2017 </h4>
We hebben vandaag presentaties gehouden. Het ging erg goed, alleen hadden we de tijdsindeling niet helemaal goed geregeld waardoor ik niet alles kon vertellen wat ik wilde vertellen. De feedback was erg positief, alleen moesten we nog werken aan hoe we de studenten met elkaar weten te verbinden.

<h4> 9 oktober 2017 </h4>
We begonnen de dag met de briefing voor iteratie 3. Het hield in dat we onze feedback moesten verwerken, onderzoek doen wanneer nodig en een pitch voorbereiden voor na de vakantie. We begonnen met de feedback verwerken wat we al erg snel hadden gedaan. vervolgens hebben we de spelregels veranderd en een planning gemaakt. Toen hebben we onze paper prototype aangepast op basis van de veranderingen.

<h4> 11 oktober 2017 </h4>
Vandaag hebben we de ontwerpproceskaart voor de 2e en 3e iteratie gemaakt. Daarnaast hebben we om feedback over ons concept gevraagd wat erg positief werd ontvangen. Met een paar aanpassingen konden we ons volledig focussen op de presentatie
<h4> 20 oktober 2017 </h4>
In de vakantie hebben we met ons team afgesproken om de pitch te schrijven. We hebben eerst twee pitches geschreven, een erg verkopend en een erg duidelijk. Vervolgens hebben we deze samengevoegd zodat de pitch een duidelijke maar ook overtuigende kracht heeft. We hebben ook besloten om onze pitch te verwerken in een video. Ik heb deze video op me genomen, maar heb wel met overleg met het groepje mijn storyboard in elkaar gezet. We wilden dit niet de maandag na de vakantie doen, omdat we graag feedback op onze pitch wilde vragen en deze in de ochtend konden verwerken. 
<h4> 23 oktober 2017 </h4>
Nadat we hele positieve feedback op onze pitch hebben gekregen ben ik de rest van de dag aan het filmpje wezen werken.
<h4> 25 oktober 2017 </h4>
Vandaag hadden we de presentaties. 's ochtends hebben we onze tafel versierd en alles klaargezet. Eenmaal van start waren onze medestudenten over het algemeen super enthousiast over zowel onze presentatie en ons concept. We hebben tot onze verbazing heel wat stickers gekregen, wel 22! Het was heel leuk om van alle groepjes te horen wat zij gemaakt en bedacht hadden. 
FEEDBACK DAG

<h4> 14 november 2017 </h4>
We moesten vandaag verzamelen in het paard van Troje voor de kick-off van design challenge twee. We moeten de opdracht maken voor het bedrijf Fabrique, een designbureau in Rotterdam, Delft en Amsterdam met als opdrachtgever Het Paard. Het probleem is dat het aantal mensen wat maar éénmalig naar Het Paard komt heel erg toenemen. Wij moeten iets digitaals gaan ontwerpen om dit tegen te gaan. De doelgroepen worden nog aangewezen. Daarna hebben we nog een film gekeken over design thinking.

<h4> 15 november </h4>

We begonnen de dag met het vinden van de groepjes en hiermee een korte kennismaking. Daarnaast hebben we nog een swot-analyse gemaakt en een google drive, whatsapp-groep en trello aangemaakt.

<h4> 20 november 2017 </h4>

Vandaag zijn we begonnen met een korte bespreking wat er op de planning staat en wat we deze en komende week in moeten leveren. Ik heb in het weekend de debrief gemaakt en deze vandaag ingeleverd op n@tschool. Ik heb nog kort geholpen met de merkanalyse, maar ben voornamelijk bezig geweest met het opstellen van een onderzoek. Ik heb vooral gekeken wat we precies te weten willen komen, waarna Benny de onderzoeksmethode hierbij heeft toegevoegd.

<h4> 21 november 2017 </h4>
ziek
<h4> 23 november 2017 </h4>
Het werkcollege van vandaag ging over een fragment uit southpark, waarbij Cartman niet een boek niet wilden lezen binnen zijn projectgroep. Hierbij moesten we verschillende soorten motivatietheorieën koppelen aan delen van het filmpje. Hierbij hebben we eigenlijk geleerd hoe je motivatietheorieën van het hoorcollege van dinsdag in de praktijk werken.

<h4> 27 november 2017 </h4>
ziek
<h4> 28 november 2017 </h4>
Het hoorcollege van vandaag ging over emotionele beïnvloeding. We hebben drie verschillende theorieën besproken die je emoties kunnen beïnvloeden.
<h4> 29 november 2017 </h4>
We hebben vandaag de drie concepten afgerond en ik heb daarbij een testplan gemaakt. Voor het testplan heb ik heel erg gekeken naar welke informatie er nodig was om te laten zien wat onze doelgroep belangrijk en onbelangrijk vindt, of wat ze nog missen in de concepten. Dit testplan heet Lilian uiteindelijk gebruikt met het onderzoek.
<h4> 30 november 2017 </h4>
In het werkcollege van vandaag ging het over een zonnewekker van philips en daarbij de emotionele beïnvloegingen die er spelen. We moesten allereerst een poster maken met de soorten beïnvloedingen die we gevonden hadden bij de lamp. Vervolgens moesten we deze theorieën gebruiken om de lamp te herontwerpen. Samen met mijn groepje waren we op het idee te komen om de lamp als plafond-lamp te gebruiken zodat heel je kamer belicht wordt in plaats van een klein deel er van.
<h4> 4 december 2017 </h4>
Ik heb vandaag gewerkt aan de recap. Ik heb een format gemaakt en daarbij alle benodigde informatie aan toegevoegd. Benny of Sander gaat hem verder ontwikkelen in InDesign of een dergelijk programma. Ik heb nog geen ervaring met dit soort programma's dus laat ik het liever aan mijn teamgenoten over. In de middag ben ik samen met Lilian en Benny gaan kijken naar de testresultaten en hierbij hebben we ons concept bedacht en deze verwerkt naar de wensen van de doelgroep.
<h4> 6 december 2017 </h4>
Voor vandaag heb ik de pitch voorbereid en deze ook voorgedragen aan de opdrachtgever en leraren. Het voorbereiden van de pitch ging erg soepel en mijn groepje vond het een duidelijke en sterke pitch zoals hij geschreven was. Bij het opdragen van de pitch ging het minder goed, ik struikelde over mijn woorden waardoor ik niet alles wat ik wilde vertellen kon zeggen. Uiteindelijk is het belangrijkste overgekomen dus het was geen grote ramp. De feedback die we op ons concept kregen was dat de app te veel leek op de website van Paard verwerkt in een app. Wel goed vond de opdrachtgever dat we gebruik maakte van een nieuwsbrief, omdat onze doelgroep een app installeren en daarbij een account aanmaken te veel zou vinden voor een eerste keer bezoeken. Dit sluit alleen niet goed aan bij ons onderzoek, waarin wij ondervonden dat de doelgroep juist niet vrolijk van een nieuwsbrief wordt. Hier willen wij onze opdrachtgever nog op aanspreken, want uiteindelijk is het een ontwerp voor de doelgroep. De pitch is uiteindelijk goed ontvangen, ik kreeg feedback dat de opbouw erg sterk was en dat alles wat ik vertelde duidelijk was. De pitch is in de middag gevalideerd door Jantien.

<h4> 11 december 2017 </h4>

We hebben voor vandaag de recap moeten inleveren, waar we feedback op hebben gekregen. Het was de eerste recap die we ooit hebben gemaakt dus we verwachtte al wat feedback. Allereerst dachten we dat het niet kort en bondig genoeg zou zijn, maar dat de informatie goed en gericht genoeg zou zijn. Uiteindelijk kwam dit goed mee overeen met de feedback van Jantien. Ze zei namelijk dat de recap relevant is voor de opdrachtgever, maar dat we één toon moeten aanhouden. Daarnaast miste nog een duidelijke conclusie met daarbij de vervolgstappen. Voor de rest kon de opmaak een heel stuk beter, maar dat was voor deze keer niet belangrijk. Ook heb ik vandaag feedback gekregen op mijn oefen starrt die ik over mijn debrief had geschreven. Ik had erg veel moeite over wat ik precies in een starrt over een debrief moest zetten. Het was dan ook niet helemaal voldoende.

<h4> 13 december 2017 </h4>

Vandaag begonnen we met studie-coaching, waar we instructies kregen over ons ik-profiel en de starrs die we vrijdag moeten inleveren. Vervolgens hebben we hier de hele ochtend aan gewerkt. In de middag heb ik samen met Benny de lay-out van de recap in elkaar gezet en ben vervolgens door gaan werken aan mijn ik-profiel.

<h4> 18 december 2017 </h4>

Ik ben vandaag begonnen aan de design rationale. Best een lastige opdracht, want ik weet niet precies in wat voor toon je het moet schrijven. We hebben vandaag van Lilian te horen gekregen dat ze de studie echt niet meer ziet zitten, ze had al aangegeven dat ze ging stoppen. Verder is er niet veel gebeurt.

<h4> 20 december 2017 </h4>
Ik heb vandaag de design rationale afgemaakt en heb feedback op mijn oefen-starrt gehad. De starrt ging over de debrief die ik aan het begin van het project heb gemaakt. De feedback was dat de basis er in stond, maar dat het nog wat uitgebreider kon, dat het meer leek alsof ik een soort checklist was afgegaan van wat er in moest i.p.v. een verhaal ervan maken.
<h4> 08 januari 2018 </h4>
Ik heb vandaag samen met Ashana gewerkt aan het prototype. We hebben eerst een schets gemaakt met wat er allemaal in moet. Vervolgens heet Ashana deze verwerkt in photoshop waarna ik hem klikbaar heb gemaakt. Dit was erg lastig, want ik heb hier nog geen ervaring mee. Uiteindelijk heb ik met Photoshop de vakken klikbaar gemaakt. Uiteindelijk heb ik er een web preview van kunnen maken, alleen zaten er witte lijnen door wat ik niet weg kreeg, deze kwamen van het selecteren van wat klikbaar moet worden.
<h4> 10 januari 2018 </h4>
Vandaag hebben we met het team besloten om thuis te blijven om te leren voor het tentamen van morgen.
<h4> 15 januari 2018 </h4>
Ik heb vandaag de nieuwsbrief afgemaakt, zodat het klaar is voor de expo. In de tussentijd heb ik Sander geholpen bij het maken van een testplan. Ook heb ik een begin gemaakt aan de pitch. In de middag moest ik eerder weg i.v.m. een röntgenfoto die ik van mijn voet moest laten maken.